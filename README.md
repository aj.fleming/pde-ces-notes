# Notes and Examples for PDE
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgit.rwth-aachen.de%2Fsisc-squad%2Fpde-ces-notes/master)

This project is meant to hold notes, examples, and other code snippets relevant to Dr. Krumscheid's "Numerical Methods for Partial Differential Equations" course. This project was started after winter semester 2020.

---

The project's directory structure is split in two:

- `/examples`, meant to hold code snippets and helper programs relevant to the course
- `/notes`, meant to hold programs and files that demonstrate core concepts (not uploads of pdfs from Moodle)

---

[RWTHJupyter](https://jupyter.pages.rwth-aachen.de/documentation/FAQ.html) is available to anybody with an RWTH TIM-ID! You can easily download Jupyter notebooks from this project and upload them to RWTHJupyter. Notebooks you store here will be persistent to use, unlike Binder notebooks.
